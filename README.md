ABOUT
=====

All the full resolution channel logos and their link to the actual channel (=serviceref) are kept up2date in this repo. The end result are picons for Enigma2 based tuners and XBMC powered by the XVDR-addon.

GUIDELINES
==========

Below are some guidelines to keep in mind if you would like to contribute, when looking at the files `tv.srindex` and `radio.srindex`, you'll get an idea on what to do next...

__Naming:__

- LOWERCASE
- NO spaces, fancy symbols or `.-+_`, except for the exceptions below
- Time sharing channels are seperated by `_`
- Sometimes it's useful to add a country code, do it by putting `-gbr`, `-deu` or `-...` at the end of the name. Country codes can be found [here](ftp://ftp.fu-berlin.de/doc/iso/iso3166-countrycodes.txt)
- If the channelname contains a `+`, use `+`, if it's a timeshift channel, use `plus1`
- Make sure you add the `# channelname` tag in `tv.srindex` or `radio.srindex` for every logo, if no serviceref is available add `--- NO DATA`
- Sorted `A-Z` as best as possible

__Serviceref:__

- UPPERCASE
- Only the part `296_5_85_C00000` is used
- `C00000` is the orbital position of the satellite, different orbital positions are grouped togheter with `--- 28.2E` as an example, see below for the `SERVICEREF ORBITAL INDEX`

__Logo:__

- LOWERCASE
- Correct name according to `tv.srindex` and `radio.srindex`
- Filetype `svg` is the way to go, otherwise `png`
- The resolution doesn't matter for `svg`, for `png` try to get it > 256px
- Quality should be as high as possible with transparancy
- A `white` version of a logo, should be placed in the folder `/tv/white` or `/radio/white`, a `black` version must always exist, a `white` version is optional

SERVICEREF ORBITAL INDEX
========================

```

	82*       ->  --- 13.0E
	C0*       ->  --- 19.2E
	EB*       ->  --- 23.5E
	11A*      ->  --- 28.2E
	...       ->  --- ...
	FFFF0000  ->  --- DVB-C GERMANY
	          ->  --- ...
	EEEE0000  ->  --- DVB-T ...
	
```
